#include "player.h"


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {

    testingMinimax = false;

     
     myBoard = Board();
     mySide = side;
     
}

Player::Player(Board* board, Side side)
{
    myBoard = *board;
    mySide = side;
}

/*
 * Destructor for the player.
 */
Player::~Player() 
{
	
}


	
/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    int score = (mySide == WHITE) ? myBoard.countWhite(): myBoard.countBlack();
    // Record move onto board tracker
    myBoard.doMove(opponentsMove, ((mySide == WHITE) ? BLACK : WHITE));
    
    // Generate our optimized move, record it on tracker
    Move *myMove = alphabeta(&myBoard, mySide, -INFINITY, INFINITY, score, 0).move;
    myBoard.doMove(myMove, mySide);

	// Play move
    return myMove;
}

branch Player::alphabeta(Board *board, Side side,int alpha, int beta, int score, int tier) 
{
    branch currBranch;
    currBranch.alpha = alpha;
    currBranch.beta = beta;
    currBranch.move = NULL;
    if(tier == 6)
    {
        int points = (side == WHITE) ? (board->countBlack() - score) : (board->countWhite() - score);
        if(side == mySide)
        {
            currBranch.alpha = points;
        }
        else
        {
            currBranch.beta = points;
        }
        return currBranch;
    }
    
    // Generate a valid move and branch to it, keeping track of 
    // change in score
    bool validmove = false;
    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            Move toCheck(i,j);
            if(board->checkMove(&toCheck, side))
            {
                validmove = true;
                Board *nextBoard = board->copy();
                Move *nextMove = new Move(i,j);
                int currScore = (side == WHITE) ? board->countWhite(): board->countBlack();
                // Edge case heuristics
                if (i == 1 || i == 6 || j == 1 || j == 6)
				{
					if ((i == 1 || i == 6)  && (j == 1 || j == 6))
					{
						currScore = pow(currScore * (tier + 1)/ 16, 3);
					}
					else if (i != 0 && i != 7 && j != 0 && j != 7)
					{currScore = pow(currScore * (tier + 1) / 4, 3);}
					else
					{currScore = pow(currScore * (tier + 1)/ 16, 3);}
					
				}
                else if (i == 0 || i == 7 || j == 0 || j == 7)
                {
					 if ((i == 0 || i == 7)  && (j == 0 || j == 7))
					 {
						 currScore *= currScore * pow((16 / (tier + 1)), 3);
					 }
					 else {currScore *= currScore * pow(4 / (tier + 1), 3);}
				}
				
                nextBoard->doMove(nextMove, side);
                branch nextbranch = alphabeta(nextBoard, ((side == WHITE) ? BLACK : WHITE), 
                                    currBranch.alpha, currBranch.beta, currScore, tier + 1);
                if(side == mySide)
                {
                    if(nextbranch.beta > currBranch.alpha)
                    {
                        currBranch.alpha = nextbranch.beta;
                        if(tier == 0)
                        {
                            Move *temp = currBranch.move;
                            currBranch.move = nextMove;
                            delete temp;
                        }
                        if(currBranch.alpha > currBranch.beta)
                        {
                            currBranch.alpha = INFINITY;
                            currBranch.beta = -INFINITY;
                            return currBranch;
                        }
                    }
                    else
                    {
                        delete nextMove;
                    }
                }
                else
                {
                    if(nextbranch.alpha < currBranch.beta)
                    {
                        currBranch.beta = nextbranch.alpha;
                        if(currBranch.alpha > currBranch.beta)
                        {
                            currBranch.alpha = INFINITY;
                            currBranch.beta = -INFINITY;
                            return currBranch;
                        }
                    }
                    else
                    {
                        delete nextMove;
                    }
                }
            }
        }
    }
    if(not validmove)
    {
        int points = (side == WHITE) ? (board->countBlack() - score) : (board->countWhite() - score);
        if(side == mySide)
        {
            currBranch.alpha = points;
        }
        else
        {
            currBranch.beta = points;
        }
        return currBranch;
    }
    return currBranch;
}

void Player::setboard(Board* board)
{
    myBoard = *board;
}
