#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#define INFINITY 999999
using namespace std;

struct branch {
    Move *move;
    int alpha;
    int beta;
};

/* struct moveNode {
	moveNode * parent;
	vector<moveNode *> children;
	Board * move
	int alpha;
	int beta;
	int score;
}
	

class gameTree {

public:

	moveNode * firstMove;
	moveNode * addMove(Move * currMove, Move * newMove);
} */
	

class Player {
	
private:

	Board myBoard;
	Side mySide;
	branch alphabeta(Board *board, Side side,int alpha, int beta, int score, int tier);
	// gameTree myTree;

public:
    Player(Side side);
    Player(Board* board, Side side);
    
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    void setboard(Board* board);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
