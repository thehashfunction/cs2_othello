Jack coded the basic implementation of alphabeta which could defeat 
SimplePlayer. Brian wrote the supporting members of Player, as well as
the improvements to the AI heuristics which allowed it to defeat
ConstantTimePlayer and BetterPlayer. 

In order to improve our AI, we implemented a recursive
alphabeta pruning to save time and space. Further, we included edge-case
heuristics which not only took into account the advantage of positions
on or neighboring the edge of the board, but also how late into the game
such a position could be taken. We argued that such positions
taken earlier in the game could be more easily recovered from, which 
justified giving more weight to positions reached further down the tree.
Under this assumption we guessed we should adjustment by a factor

 (a * tier) ^ b
 
where a is some numerical measure of the advantage of a position and b
represents an assessment parameter. b is negative or position depending
on which side the advantage goes to. We adjusted parameters until our AI
could consistently beat ConstantTimePlayer, but for some time we could 
not get past BetterPlayer, always losing 28-36. Watching the games we 
noticed we were taking edge positions neighboring corners much too often,
and looking over the code again we realized our code treated those
corners as positively advantageous and therefore was making dumb moves.
We included these positions in the negatively advantageous category and
began whooping butt.  
